﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01.NumberOfCoinsNeedForSum
{
    class NumberOfCoinsNeedForSum
    {
        static void Main()
        {
            string input = "5 2 8"; // it has a precise solution but not with greedy and it is 3 coins x 8 + 1 coins x 5 + 2 coins x 2 == 33
            string endValueOfCoins = "33";
            string[] coinsAsString = input.Split(' ');
            int[] coins = new int[coinsAsString.Length];
            for (int i = 0; i < coinsAsString.Length; i++)
            {
                coins[i] = int.Parse(coinsAsString[i]);
            }
            int endValue = int.Parse(endValueOfCoins);

            Greedy(coins, endValue);

        }

        private static void Greedy(int[] coins, int endValue)
        {
            Array.Sort(coins, (x,y) => y.CompareTo(x));
            Dictionary<int, int> coinsAmount = new Dictionary<int, int>();
            int totalAmount = 0;
            foreach (var coin in coins)
            {
                int amount = 0;
                while (totalAmount + (amount + 1) * coin <= endValue)
                {
                    amount++;
                }

                if (amount != 0)
                {
                    coinsAmount.Add(coin, amount);
                    totalAmount += amount * coin;
                    if (totalAmount == endValue)
                    {
                        break;
                    }
                }
            }

            Console.WriteLine("Total achieved sum is: {0}",totalAmount);
            Console.WriteLine("Coins are:");
            StringBuilder sb = new StringBuilder();
            foreach (var coin in coinsAmount)
            {
                sb.Append(string.Format("{1} coins x {0} + ", coin.Key, coin.Value));
            }
            sb.Length -= 3;
            Console.WriteLine(sb.ToString());
        }
    }
}
