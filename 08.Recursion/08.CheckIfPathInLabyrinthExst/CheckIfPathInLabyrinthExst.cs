﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08.CheckIfPathInLabyrinthExst
{
    class CheckIfPathInLabyrinthExst
    {
        internal static bool existPath = false;
        
        public static char[,] labyrinth;

        public static void Main()
        {
            labyrinth = new char[,]
            {
                {' ', ' ', ' ', '*', ' ', ' ', ' '},
                {'*', '*', ' ', '*', ' ', '*', ' '},
                {' ', ' ', ' ', ' ', ' ', ' ', ' '},
                {' ', '*', '*', '*', '*', '*', ' '},
                {' ', ' ', ' ', ' ', ' ', ' ', ' '},
            };

            // test with empty array [100, 100]
            /*labyrinth = new char[100, 100];
            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    labyrinth[i, j] = ' ';
                }
            }*/

            Tuple<int, int> startPosition = new Tuple<int, int>(0, 0);
            Tuple<int, int> endPosition = new Tuple<int, int>(4, 6);
            FindAllPaths(startPosition, endPosition);

            Console.WriteLine(existPath);
        }

        public static bool IsInRange(Tuple<int, int> currentPosition)
        {
            if (currentPosition.Item1 < 0 || currentPosition.Item1 >= labyrinth.GetLength(0))
            {
                return false;
            }
            if (currentPosition.Item2 < 0 || currentPosition.Item2 >= labyrinth.GetLength(1))
            {
                return false;
            }

            return true;
        }

        private static void FindAllPaths(Tuple<int, int> currentPosition, Tuple<int, int> endPosition)
        {
            if (IsInRange(currentPosition) && !existPath)
            {
                if (labyrinth[currentPosition.Item1, currentPosition.Item2] != ' ')
                {
                    // The current cell is not free -> can't find a path
                    return;
                }

                if (currentPosition.Item1 == endPosition.Item1 && currentPosition.Item2 == endPosition.Item2)
                {
                    existPath = true;
                    return;
                }

                labyrinth[currentPosition.Item1, currentPosition.Item2] = '*';
                FindAllPaths(new Tuple<int, int>(currentPosition.Item1 + 1, currentPosition.Item2), endPosition);
                FindAllPaths(new Tuple<int, int>(currentPosition.Item1 - 1, currentPosition.Item2), endPosition);
                FindAllPaths(new Tuple<int, int>(currentPosition.Item1, currentPosition.Item2 + 1), endPosition);
                FindAllPaths(new Tuple<int, int>(currentPosition.Item1, currentPosition.Item2 - 1), endPosition);
                labyrinth[currentPosition.Item1, currentPosition.Item2] = ' ';
            }
        }
    }
}
