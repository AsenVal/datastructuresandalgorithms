﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04.PrintAllPermutations
{
    public class PrintAllPermutations
    {
        public static void Main()
        {
            Console.WriteLine("Insert a number of element of set n:");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            GenerateAllCombinations(0, arr);
        }

        private static void GenerateAllCombinations(int index, int[] arr)
        {
            int start;
            if (index == arr.Length)
            {
                Console.Write("{");
                Console.Write(string.Join(", ", arr));
                Console.WriteLine("}");
            }
            else
            {
                for (int i = 1; i <= arr.Length; i++)
                {
                    bool checkNumber = true;
                    for (int j = 0; j < index; j++)
                    {
                        if (arr[j] == i)
                        {
                            checkNumber = false;
                        }
                    }
                    if (checkNumber)
                    {
                        arr[index] = i;
                        GenerateAllCombinations(index + 1, arr);
                    }
                }
            }
        }
    }
}
