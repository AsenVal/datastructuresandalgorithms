﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.PreviousToSkipDublications
{
    public class PreviousToSkipDublications
    {
        public static void Main()
        {
            Console.WriteLine("Insert a number of combination elements k:");
            int k = int.Parse(Console.ReadLine());
            Console.WriteLine("Insert a number of element of set n:");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[k];
            GenerateAllCombinations(0, n, arr);
        }

        private static void GenerateAllCombinations(int index, int n, int[] arr)
        {
            int start;
            if (index == arr.Length)
            {
                Console.WriteLine(string.Join(", ", arr));
            }
            else
            {
                if (index != 0)
                {
                    start = arr[index - 1] + 1;
                }
                else
                {
                    start = 1;
                }

                for (int i = start; i <= n; i++)
                {
                    arr[index] = i;
                    GenerateAllCombinations(index + 1, n, arr);
                }
            }
        }
    }
}
