﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01.RecursionFornLoopN
{
    public class RecursionFornLoopN
    {
        public static void Main()
        {
            Console.WriteLine("Insert a number:");
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            GenerateAllCombinations(0, arr);
        }

        private static void GenerateAllCombinations(int index, int[] arr)
        {
            if (index == arr.Length)
            {
                Console.WriteLine(string.Join(", ", arr));
            }
            else
            {
                for (int i = 1; i <= arr.Length; i++)
                {
                    arr[index] = i;
                    GenerateAllCombinations(index + 1, arr);
                }
            }
        }
    }
}
