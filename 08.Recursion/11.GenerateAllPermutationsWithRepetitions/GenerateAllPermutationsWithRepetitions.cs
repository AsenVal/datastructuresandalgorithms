﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11.GenerateAllPermutationsWithRepetitions
{
    public class GenerateAllPermutationsWithRepetitions
    {
        static void Main(string[] args)
        {
            //int[] numbers = { 1, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
            int[] numbers = { 1, 3, 5, 5 };
            PermutationsWithRepetition(numbers);
        }

        static void PermutationsWithRepetition(int[] numbersSet)
        {
            Array.Sort(numbersSet);
            Permute(numbersSet, 0, numbersSet.Length);
        }

        static void Permute(int[] arr, int start, int end)
        {
            Console.Write("{");
            Console.Write(string.Join(", ", arr));
            Console.WriteLine("}");
            
            int swapValue = 0;

            if (start < end)
            {
                for (int i = end - 2; i >= start; i--)
                {
                    for (int j = i + 1; j < end; j++)
                    {
                        if (arr[i] != arr[j])
                        {
                            swapValue = arr[i];
                            arr[i] = arr[j];
                            arr[j] = swapValue;

                            Permute(arr, i + 1, end);
                        }
                    }

                    swapValue = arr[i];
                    for (int k = i; k < end - 1; k++)
                    {
                        arr[k] = arr[k + 1];
                    }
                    arr[end - 1] = swapValue;
                }
            }
        }
    }
}
