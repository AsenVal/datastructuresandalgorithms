﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.ColouredRabbits
{
    public class ColouredRabbits
    {
        public static void Main()
        {
            Dictionary<int, int> rabbits = new Dictionary<int, int>();
            int n = int.Parse(Console.ReadLine());
            int row;
            for (int i = 0; i < n; i++)
            {
                row = int.Parse(Console.ReadLine());
                if (rabbits.ContainsKey(row))
                {
                    rabbits[row]++;
                }
                else
                {
                    rabbits.Add(row, 1);
                }
            }

            int count = 0;
            foreach (var rabbit in rabbits)
            {
                if (rabbit.Value <= rabbit.Key + 1)
                {
                    count += rabbit.Key + 1;
                }
                else
                {
                    count += (int)Math.Ceiling((double)(rabbit.Value) / (rabbit.Key + 1)) * (rabbit.Key + 1);
                }
            }
            Console.WriteLine(count);
        }
    }
}
