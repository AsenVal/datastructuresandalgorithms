﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07.Circles
{
    public class Circles
    {
        static int count = 0;
        static List<string> combinations = new List<string>();
        static HashSet<string> allCombinations = new HashSet<string>();

        internal static void Main()
        {
            string line = Console.ReadLine();
            char[] colors = line.ToCharArray();

            //Array.Sort(colors);
            //PermuteRep(colors, 0, colors.Length);
            if (colors.Length == 1)
            {
                Console.WriteLine("1");
                return;
            }
            PermuteRep(colors, 1);
            Console.WriteLine(combinations.Count);
        }

        static void PermuteRep(char[] arr, int k)
        {
            if (k == arr.Length - 1)
            {
                AddCombination(arr);
                return;
            }

            PermuteRep(arr, k + 1);
            for (int i = k + 1; i < arr.Length; i++)
            {
                if (arr[k] != arr[i])
                {
                    Swap(ref arr[k], ref arr[i]);
                    PermuteRep(arr, k + 1);
                    Swap(ref arr[k], ref arr[i]);
                }
            }
        }

        
        /*private static bool CheckForRotationAndSimetry(char[] arr)
        {
            foreach (var combination in combinations)
			{
                for (int i = 0; i < arr.Length; i++)
                {
                    bool isEqual = true;
                    int k = 0;
                    for (int j = i; j < arr.Length; j++, k++)
                    {
                        if (arr[j] != combination[k])
                        {
                            isEqual = false;
                        }
                    }

                    for (int j = 0; j < i; j++, k++)
                    {
                        if (arr[j] != combination[k])
                        {
                            isEqual = false;
                        }
                    }

                    if (isEqual)
                    {
                        return true;
                    }

                    isEqual = true;
                    k = 0;
                    for (int j = i; j < arr.Length; j++, k++)
                    {
                        if (arr[j] != combination[combination.Length - 1 - k])
                        {
                            isEqual = false;
                        }
                    }

                    for (int j = 0; j < i; j++, k++)
                    {
                        if (arr[j] != combination[combination.Length - 1 - k])
                        {
                            isEqual = false;
                        }
                    }

                    if(isEqual)
                    {
                        return true;
                    }                    
                }
			}
 	        return false;
        }*/


        private static void AddCombination(char[] arr)
        {
            
            string newComb = new string(arr);
            if(!allCombinations.Contains(newComb))
            {
                char[] newBalls = new char[arr.Length];
                char[] reversedNewBalls = new char[arr.Length];
                
                for (int i = 0; i < arr.Length; i++)
                {
                    for (int j = i; j < arr.Length; j++)
                    {
                        newBalls[j - i] = arr[j];
                        reversedNewBalls[arr.Length - j + i - 1] = arr[j];
                    }

                    for (int j = 0; j < i; j++)
                    {
                        newBalls[arr.Length - i + j] = arr[j];
                        reversedNewBalls[i - j - 1] = arr[j];
                    }
                    allCombinations.Add(new string(newBalls));
                    allCombinations.Add(new string(reversedNewBalls));
                }

                combinations.Add(newComb);
            }
        }

        static void Swap<T>(ref T first, ref T second)
        {
            T oldFirst = first;
            first = second;
            second = oldFirst;
        }
    }
}
