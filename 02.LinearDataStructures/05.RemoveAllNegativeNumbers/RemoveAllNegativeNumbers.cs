﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _05.RemoveAllNegativeNumbers
{
    class RemoveAllNegativeNumbers
    {
        static void Main()
        {
            var numbers = ReadNumbers();
            numbers = RemoveNegative(numbers);
            PrintNumbers(numbers);
        }

        private static LinkedList<int> RemoveNegative(LinkedList<int> numbers)
        {
            var node = numbers.First;
            while (node != null)
            {
                var next = node.Next;
                if (node.Value < 0)
                {
                    numbers.Remove(node);
                }
                node = next;
            }
            return numbers;
        }

        private static void PrintNumbers(LinkedList<int> numbers)
        {
            Console.WriteLine("Print numbers.");
            Console.WriteLine(string.Join(",", numbers));
        }

        private static LinkedList<int> ReadNumbers()
        {
            int n;
            string str;
            LinkedList<int> numbers = new LinkedList<int>();
            do
            {
                Console.WriteLine("Enter number:");
                str = Console.ReadLine();
                if (int.TryParse(str, out n))
                {
                    numbers.AddLast(n);
                }
            }
            while (str != "");

            return numbers;
        }
    }
}
