﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11.ImplementingLinkedList
{
    public class ListItem<T>
    {
        private T value;
        private ListItem<T> nextItem;
        public T Value
        {
            set
            {
                this.value = value;
            }
            get
            {
                return this.value;
            }
        }

        public ListItem<T> NextItem
        {
            get
            {
                return this.nextItem;
            }
            set
            {
                this.nextItem = value;
            }
        }

        public ListItem(T value, ListItem<T> nextItem = null)
        {
            this.Value = value;
            this.NextItem = nextItem;
        }
    }
}
