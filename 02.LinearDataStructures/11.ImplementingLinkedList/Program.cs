﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11.ImplementingLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            UserLinkedList<int> linkedList = new UserLinkedList<int>();
            linkedList.AddFirst(50);
            linkedList.AddLast(22);
            linkedList.AddFirst(20);
            linkedList.AddLast(515);
            linkedList.AddAfter(linkedList.FirstElement.NextItem.NextItem, 1000);
            linkedList.AddBefore(linkedList.FirstElement.NextItem.NextItem.NextItem, 2000);
            linkedList.AddBefore(linkedList.FirstElement, 0);
            linkedList.RemoveFirst();
            linkedList.RemoveLast();

            ListItem<int> next = linkedList.FirstElement;
            while (next != null)
            {
                Console.WriteLine(next.Value);
                next = next.NextItem;
            }

            Console.WriteLine("Number of items.");
            Console.WriteLine(linkedList.Count);
            linkedList.Clear();
            Console.WriteLine("Number of items after clear.");
            Console.WriteLine(linkedList.Count);
        }
    }
}
