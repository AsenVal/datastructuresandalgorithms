﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01.ListCalcSumAndAvarage
{
    public class CalcSumAndAvarage
    {
        static void Main()
        {
           var numbers = ReadNumbers();
           Console.WriteLine(CalcSum(numbers));
           Console.WriteLine(CalcAvarage(numbers));
        }

        private static string CalcAvarage(List<int> numbers)
        {
            double avarage = (double)numbers.Sum() / numbers.Count;
            avarage = Math.Round(avarage, 3);
            return avarage.ToString();
        }

        private static string CalcSum(List<int> numbers)
        {
            int sum = numbers.Sum();
            return sum.ToString();
        }

        private static List<int> ReadNumbers()
        {
            int n;
            string str;
            List<int> numbers = new List<int>();
            do
            {
                Console.WriteLine("Enter number:");
                str = Console.ReadLine();
                if (int.TryParse(str, out n))
                {
                    numbers.Add(n);
                }
            }
            while (str != "");

            return numbers;
        }
    }
}
