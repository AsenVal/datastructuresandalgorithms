﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using _04.ListFindLongestSubsequence;

namespace _04ListFindLongesSubsequenceTest
{
    [TestClass]
    public class FindLongestSequenceTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void LongestSequenceEmpty()
        {
            var list = new List<int>();
            var result = FindLongestSubsequence.LongestSequence(list);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void LongestSequenceNull()
        {
            var result = FindLongestSubsequence.LongestSequence(null);
        }

        [TestMethod]
        public void LongestSequenceOneElement()
        {
            var list = new List<int>(){5};
            var result =  FindLongestSubsequence.LongestSequence(list);
            Assert.AreEqual(5,result[0]);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod]
        public void LongestSequenceThreeEqualElement()
        {
            var list = new List<int>() { 5, 5, 5 };
            var result = FindLongestSubsequence.LongestSequence(list);
            Assert.AreEqual(5, result[0]);
            Assert.AreEqual(3, result.Count);
        }



        [TestMethod]
        public void LongestSequenceDifferentElements()
        {
            var list = new List<int>() { 5, 6, 6, 6, 7, 8, 8, 5, 5, 9, 15, 6 };
            var result = FindLongestSubsequence.LongestSequence(list);
            Assert.AreEqual(6, result[0]);
            Assert.AreEqual(3, result.Count);
        }
    }
}
