﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06.DictionaryRemoveNumbersOccuredOddTimes
{
    class RemoveNumbersOccuredOddTimes
    {
        static void Main()
        {
            List<int> numbers = ReadNumbers();
            //List<int> numbers = new List<int>() { 4, 2, 2, 5, 2, 3, 2, 3, 1, 5, 2 };
            Dictionary<int, int> occures = new Dictionary<int, int>();
            foreach (var item in numbers)
            {
                if (occures.ContainsKey(item))
                {
                    occures[item]++;
                }
                else
                {
                    occures.Add(item, 1);
                }
            }
            numbers.RemoveAll(x => occures[x] % 2 != 0);
            PrintNumbers(numbers);
        }

        private static void PrintNumbers(List<int> numbers)
        {
            Console.WriteLine("Print numbers.");
            Console.WriteLine(string.Join(",", numbers));
        }

        private static List<int> ReadNumbers()
        {
            int n;
            string str;
            List<int> numbers = new List<int>();
            do
            {
                Console.WriteLine("Enter number:");
                str = Console.ReadLine();
                if (int.TryParse(str, out n))
                {
                    numbers.Add(n);
                }
            }
            while (str != "");

            return numbers;
        }
    }    
}
