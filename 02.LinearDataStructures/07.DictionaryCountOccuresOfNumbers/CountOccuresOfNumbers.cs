﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07.DictionaryCountOccuresOfNumbers
{
    class CountOccuresOfNumbers
    {
        public static void Main()
        {
            List<int> numbers = ReadNumbers();
            //List<int> numbers = new List<int>() { 3, 4, 4, 2, 3, 3, 4, 5, 3, 2, 5, 6, 5, 6, 7, 5 };
            Dictionary<int, int> occures = new Dictionary<int, int>();
            foreach (var item in numbers)
            {
                if (occures.ContainsKey(item))
                {
                    occures[item]++;
                }
                else
                {
                    occures.Add(item, 1);
                }
            }
            PrintNumbers(occures);
        }

        private static void PrintNumbers(Dictionary<int, int> numbers)
        {
            Console.WriteLine("Print numbers.");
            foreach (var item in numbers)
	        {
		        Console.WriteLine("{0} -> {1} times",item.Key,item.Value);
            }
        }

        private static List<int> ReadNumbers()
        {
            int n;
            string str;
            List<int> numbers = new List<int>();
            do
            {
                Console.WriteLine("Enter number:");
                str = Console.ReadLine();
                if (int.TryParse(str, out n))
                {
                    numbers.Add(n);
                }
            }
            while (str != "");

            return numbers;
        }
    }
}
