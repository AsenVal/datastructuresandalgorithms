﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03.ListIncreaseSorting
{
    public class IncreaseSorting
    {
        static void Main()
        {
            var numbers = ReadNumbers();
            numbers.Sort();
            PrintNumbers(numbers);
        }

        private static void PrintNumbers(List<int> numbers)
        {
            Console.WriteLine("Sorted numbers.");
            Console.WriteLine(string.Join(",", numbers));
        }

        private static List<int> ReadNumbers()
        {
            int n;
            string str;
            List<int> numbers = new List<int>();
            do
            {
                Console.WriteLine("Enter number:");
                str = Console.ReadLine();
                if (int.TryParse(str, out n))
                {
                    numbers.Add(n);
                }
            }
            while (str != "");

            return numbers;
        }
    }
}
