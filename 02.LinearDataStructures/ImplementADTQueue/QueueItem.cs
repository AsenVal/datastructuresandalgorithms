﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _13.ImplementADTQueue
{
    public class QueueItem<T>
    {
        T value;
        QueueItem<T> nextItem;

        public QueueItem()
        {
        }

        public QueueItem(T value)
        {
            this.Value = value;
        }

        public T Value
        {
            get
            {
                return this.value;
            }
            set
            {
                this.value = value;
            }
        }

        public QueueItem<T> NextItem
        {
            get
            {
                return this.nextItem;
            }
            set
            {
                this.nextItem = value;
            }
        }
    }
}
