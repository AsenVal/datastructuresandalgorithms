﻿using System;

namespace _13.ImplementADTQueue
{
    class Program
    {
        static void Main()
        {
            LinkedQueue<int> queue = new LinkedQueue<int>();
            Random rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                queue.Enqueue(rand.Next(1,100));
            }

            Console.WriteLine(queue.Peek());
            Console.WriteLine();
            Console.WriteLine("The count of queue elements is: {0}", queue.Count);

            while(queue.Count > 0)
            {
                Console.WriteLine(queue.Dequeue());
            }

            Console.WriteLine("The count of queue elements is: {0}",queue.Count);
            Console.WriteLine();
            queue.Enqueue(26);
            Console.WriteLine(queue.Dequeue());
        }
    }
}
