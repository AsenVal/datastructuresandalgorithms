﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13.ImplementADTQueue
{
    public class LinkedQueue<T>
    {
        QueueItem<T> firstItem;
        QueueItem<T> lastItem;
        int count;

        public void Enqueue(T value)
        {
            if (firstItem == null)
            {
                lastItem = new QueueItem<T>(value);
                firstItem = lastItem;
            }
            else
            {
                lastItem.NextItem = new QueueItem<T>(value);
                lastItem = lastItem.NextItem;
            }

            count++;
        }

        public T Peek()
        {
            return firstItem.Value;
        }

        public T Dequeue()
        {
            if (firstItem == null)
            {
                throw new InvalidOperationException("Queue is empty.");
            }
            T returnValue = firstItem.Value;
            firstItem = firstItem.NextItem;
            count--;
            return returnValue;
        }

        public int Count
        {
            get
            {
                return count;
            }
        }
    }
}
