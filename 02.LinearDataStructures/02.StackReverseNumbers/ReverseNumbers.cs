﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02.StackReverseNumbers
{
    public class ReverseNumbers
    {
        static void Main()
        {
            var numbers = ReadNumbers();
            PringNumbers(numbers);
        }
  
        private static void PringNumbers(Stack<int> numbers)
        {
            Console.WriteLine("Reversed numbers.");
            Console.WriteLine(string.Join(",", numbers));
        }

        private static Stack<int> ReadNumbers()
        {
            int n;
            string str;
            Stack<int> numbers = new Stack<int>();
            do
            {
                Console.WriteLine("Enter number:");
                str = Console.ReadLine();
                if (int.TryParse(str, out n))
                {
                    numbers.Push(n);
                }
            }
            while (str != "");

            return numbers;
        }
    }
}
