﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12.ImplementADTStack
{
    public class UserStack<T>
    {
        private T[] stackArray;
        private int count;

        public int Count
        {
            get
            {
                return this.count;
            }
            private set { }
        }

        public UserStack()
        {
            this.stackArray = new T[4];
            this.Count = 0;
        }

        public void Push(T value)
        {
            if (this.Count==this.stackArray.Length)
            {
                ResizeStackStorage();
            }

            this.stackArray[this.Count] = value;
            this.count += 1;
        }

        public T Pop()
        {
            if (this.Count == 0)
            {
                throw new ArgumentNullException("OurStack is empty!");
            }
            T value = this.stackArray[this.Count-1];
            this.stackArray[this.Count - 1] = default(T);
            this.count--;

            return value;
        }

        public T Peek()
        {
            if (this.Count == 0)
            {
                throw new ArgumentNullException("OurStack is empty!");
            }
            T value = this.stackArray[this.Count-1];

            return value;
        }

        public void Clear()
        {
            this.stackArray = new T[1];
            this.count = 0;
        }

        public bool Contains(T value)
        {
            bool isContaining = false;

            for (int i = 0; i < this.Count; i++)
            {
                if (this.stackArray[i].Equals(value))
                {
                    isContaining = true;
                    break;
                }
            }

            return isContaining;
        }

        public T[] ToArray()
        {
            T[] newArray = new T[this.Count];
            for (int i = 0; i < this.Count; i++)
            {
                newArray[i] = this.stackArray[i];
            }

            return newArray;
        }

        public void TrimExcess()
        {
            T[] newArray = new T[this.Count];
            for (int i = 0; i < this.Count; i++)
            {
                newArray[i] = this.stackArray[i];
            }
           
            this.stackArray = newArray;
        }

        private void ResizeStackStorage()
        {
            T[] newStorage = new T[this.stackArray.Length * 2];
            for (int i = 0; i < this.stackArray.Length; i++)
            {
                newStorage[i] = this.stackArray[i];
            }

            this.stackArray = newStorage;
        }
    }
}
