﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12.ImplementADTStack
{
    class Program
    {
        static void Main(string[] args)
        {
            UserStack<int> stack = new UserStack<int>();
            stack.Push(11);
            stack.Push(20);
            stack.Push(52);
            stack.Push(502);
            stack.Push(9);

            int[] stackToArray = stack.ToArray();
            for (int i = 0; i < stackToArray.Length; i++)
            {
                Console.Write(stackToArray[i] + ", ");
            }

            Console.WriteLine();
            Console.WriteLine(stack.Peek());
            Console.WriteLine(stack.Pop());
            Console.WriteLine(stack.Peek());
            Console.WriteLine(stack.Contains(20));
            stack.TrimExcess();
            stack.Push(45);
            Console.WriteLine(stack.Peek());
            stack.Clear();
            Console.WriteLine(stack.Count);
        }
    }
}
