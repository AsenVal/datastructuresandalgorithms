﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01.PriorityQueueWithBinaryHeap
{
    class Program
    {
        static void Main()
        {
            PriorityQueue<int> heap = new PriorityQueue<int>(2);
            heap.Enqueue(1);
            heap.Enqueue(21);
            heap.Enqueue(3);
            heap.Enqueue(6); 
            Console.WriteLine(heap.Dequeue());
            heap.Enqueue(31);
            heap.Enqueue(5);
            heap.Enqueue(8);
            heap.Enqueue(12);
            heap.Enqueue(7);
            heap.Enqueue(5);
            heap.Enqueue(19);
            heap.Enqueue(1);
            heap.Enqueue(9);
            heap.Enqueue(11);
            heap.Enqueue(1);
            Console.WriteLine("ToString elements");
            Console.WriteLine(heap.ToString());
            Console.WriteLine("Dequeue elements");
            while (heap.Count > 0)
            {
                Console.Write("{0}, ", heap.Dequeue());
            }
        }
    }
}
