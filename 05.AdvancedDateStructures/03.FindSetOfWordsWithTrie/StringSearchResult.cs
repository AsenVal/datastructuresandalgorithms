﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.FindSetOfWordsWithTrie
{
    public class StringSearchResult
    {
        public StringSearchResult(string value, int position)
        {
            this.Value = value;
            this.Position = position;
        }

        public string Value { get; private set; }
        public int Position { get; private set; }
    }
}
