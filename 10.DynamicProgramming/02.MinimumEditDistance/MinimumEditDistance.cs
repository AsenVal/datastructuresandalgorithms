﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.MinimumEditDistance
{
    public class MinimumEditDistance
    {
        private enum Operation
        {
            Skip = 0,
            Delete = 1,
            Insert = 2,
            Replace = 3
        }

        static void Main()
        {
            /*Console.WriteLine("Insert first string:");
            string startString = Console.ReadLine();
            Console.WriteLine("Insert result string:");
            string endString = Console.ReadLine();*/
            string startWord = "developer";
            string endWord = "enveloped";
            const double DeleteCost = 0.9;
            const double InsertCost = 0.8;
            const double ReplaceCost = 1;

            FindMinEditDistance(startWord, endWord, DeleteCost, InsertCost, ReplaceCost);
        }

        private static void FindMinEditDistance(string startWord, string endWord, double deleteCost, double insertCost, double replaceCost)
        {
            int m = startWord.Length;
            int n = endWord.Length;
            
            double[,] Matrix = new double[m + 1, n + 1];
            Operation[,] operations = new Operation[m + 1, n + 1];

            // If the second string is empty then
            // we need (i) delete operations to
            // convert the first string into empty string
            for (int i = 0; i <= m; i++)
            {
                Matrix[i, 0] = i * deleteCost;
                operations[i, 0] = Operation.Delete;
            }

            // If the first string is empty then
            // we need (j) insert operations to
            // convert the first string into the second string
            for (int j = 0; j <= n; j++)
            {
                Matrix[0, j] = j * insertCost;
                operations[0, j] = Operation.Insert;
            }

            // For each substring in the first
            // string ending at position (i)
            for (int i = 1; i <= m; i++)
            {
                // For each substring in the second string
                // ending at position (j)
                for (int j = 1; j <= n; j++)
                {
                    // If the last characters in each substring
                    // are equal, then no operation is needed
                    if (startWord[i - 1] == endWord[j - 1])
                    {
                        Matrix[i, j] = Matrix[i - 1, j - 1];
                        operations[i, j] = Operation.Skip;
                    }
                    // Otherwise we take the minimum of three cases
                    else
                    {
                        // Case 1: deleting last character in
                        // the first substring ending at position (i)
                        double min = deleteCost + Matrix[i - 1, j];
                        operations[i, j] = Operation.Delete;

                        // Case 2: Inserting the last character from
                        // the second substring at position (j)
                        // into the first substring ending at position (i)
                        if (insertCost + Matrix[i, j - 1] < min)
                        {
                            min = insertCost + Matrix[i, j - 1];
                            operations[i, j] = Operation.Insert;
                        }

                        // Case 3: Replacing the last character in
                        // the first substring at position (i) with
                        // the last character in the second substring
                        // at position (j)
                        if (replaceCost + Matrix[i - 1, j - 1] < min)
                        {
                            min = replaceCost + Matrix[i - 1, j - 1];
                            operations[i, j] = Operation.Replace;
                        }

                        // Save the minimum value
                        Matrix[i, j] = min;
                    }
                }
            }

            Console.WriteLine("Total cost: {0}", Matrix[m, n]);
            PrintSteps(operations, startWord, endWord, m, n);
        }
        
        private static void PrintSteps(Operation[,] operations, string word1, string word2, int i, int j)
        {
            // Base case, do nothing as the solution will be
            // trivial. It is either (j) insertions or (i) deletions
            if (i <= 0 && j <= 0)
            {
                return;
            }

            // Deleting the character at position (i)
            if (operations[i, j] == Operation.Delete)
            {
                PrintSteps(operations, word1, word2, i - 1, j);
                Console.WriteLine("Delete character '{0}' at position {1} from {2}.", word1[i - 1], i - 1, word1);
            }
            // Inserting a character at position (i)
            else if (operations[i, j] == Operation.Insert)
            {
                PrintSteps(operations, word1, word2, i, j - 1);
                Console.WriteLine(
                    "Insert character '{0}' from {4} at position {1}" +
                    " into {3} at position {2}.",
                    word2[j - 1], j - 1, i - 1, word1, word2);
            }
            // Replacing a character at position (i)
            else if (operations[i, j] == Operation.Replace)
            {
                PrintSteps(operations, word1, word2, i - 1, j - 1);
                Console.WriteLine(
                    "Replace character '{0}' at position {1} in {2}" +
                    " with character '{3}' at position {4} in {5}.",
                    word1[i - 1], i + 1, word1, word2[j - 1], j - 1, word2);
            }
            // No operation is needed
            else
            {
                PrintSteps(operations, word1, word2, i - 1, j - 1);
            }
        }
    }
}
