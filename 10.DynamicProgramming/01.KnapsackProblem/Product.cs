﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01.KnapsackProblem
{
    public class Product: IComparable
    {
        public string Name { get; set; }
        public int Weight { get; set; }
        public int Price { get; set; }

        public Product(string name, int weight, int price)
        {
            this.Name = name;
            this.Weight = weight;
            this.Price = price;
        }

        public int CompareTo(object obj)
        {
            if (obj is Product)
            {
                Product product = (Product)obj;
                if(this.Price.CompareTo(product.Price) < 0)
                {
                    return 1;
                }
                else if (this.Price.CompareTo(product.Price) > 0)
                {
                    return -1;
                }
                else if (this.Weight.CompareTo(product.Weight) > 0)
                {
                    return 1;
                }
                else if (this.Weight.CompareTo(product.Weight) < 0)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
            throw new ArgumentException("Object is not a Product.");
        }
    }
}
