﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using Wintellect.PowerCollections;

namespace _01.KnapsackProblem
{
    public class KnapsackProblem
    {
        //static List<Product> products = new List<Product>();
        //static List<Product> currentProducts = new List<Product>();
        //static OrderedMultiDictionary<int, int> total = new OrderedMultiDictionary<int, int>(true);
        //static int maxWeight;
        static void Main()
        {
            int maxWeight;
            List<Product> products = ReadInput(out maxWeight);

            SovingKnapSackProblem(products, maxWeight);
            //KnapsackRecursion(0, 0, 0);
        }

        private static void SovingKnapSackProblem(List<Product> products, int maxWeight)
        {
            bool[,] used = new bool[maxWeight + 1, products.Count];
            int[] M = new int[maxWeight + 1];

            M[0] = 0;

            for (int j = 1; j <= maxWeight; j++)
            {
                // M[j] will be max1 (or M[j-1])
                // if the jth slot is empty
                int max1 = M[j - 1];

                // M[j] will be max2 if the jth
                // slot is occupied by some item
                // Initialize max2 to some small number
                int max2 = int.MinValue;

                // This is used to mark the previous (j)
                // slot if the jth slot is occupied
                int mark = 0;

                // This is used to keep the index
                // of the last candidate which can be put
                // in the knapsack
                int candidateUsed = 0;

                // Search for an item to occupy the jth
                // slot such that it gives us maximum value
                for (int i = 0; i < products.Count; i++)
                {
                    // For each item (i) calculate (products[i].Price + M[j - products[i].Weight)]
                    // then compare it to the current max. If it is greater
                    // then update the current max. Only those items satisfying
                    // the condition (j - products[i].Weight >= 0) are checked because capacity
                    // should not be negative
                    if (j - products[i].Weight >= 0 && !used[j - products[i].Weight, i] && products[i].Price + M[j - products[i].Weight] > max2)
                    {
                        // Update the max
                        max2 = products[i].Price + M[j - products[i].Weight];
                        // Save the previous (j) position
                        // that gives us the maximum value
                        mark = j - products[i].Weight;
                        // Update the candidate item which
                        // might be put in the knapsack
                        candidateUsed = i;
                    }
                }

                //Case1: jth slot is empty
                if (max1 > max2)
                {
                    M[j] = max1;

                    for (int k = 0; k < products.Count; k++)
                    {
                        used[j, k] = used[j - 1, k];
                    }
                }
                //Case 2: jth slot is occupied
                else
                {
                    M[j] = max2;

                    for (int k = 0; k < products.Count; k++)
                    {
                        used[j, k] = used[mark, k];
                    }

                    // mark the candidate as used, which will prevent us
                    // from putting it again in the knapsack
                    used[j, candidateUsed] = true;
                }
            }

            Console.WriteLine(
            "The maximum value we can get by filling\r\n" +
            "the knapsack with maxWeight {0} is {1}.",
            maxWeight,
            M[maxWeight]);

            for (int i = 0; i < products.Count; i++)
            {
                if (used[maxWeight, i])
                {
                    Console.WriteLine("{0} - weight={1}, cost={2}", products[i].Name, products[i].Weight, products[i].Price);
                }
            }
        }

        /*private static void KnapsackRecursion(int index, int currentPrice, int currentWeight)
        {
            for (int i = index; i < products.Count; i++)
            {
                if (products[i].Weight + currentWeight > maxWeight)
                {
                    //total.Add(currentPrice, currentProducts);
                    return;
                }

                currentProducts.Add(products[i]);
                currentWeight += products[i].Weight;
                currentPrice += products[i].Price;
                KnapsackRecursion(i + 1, currentPrice, currentWeight);
                currentWeight -= products[i].Weight;
                currentPrice -= products[i].Price;
                currentProducts.RemoveAt(currentProducts.Count - 1);
            }
        }*/

        private static List<Product> ReadInput(out int maxWeight)
        {
            int numberOfProducts = 6;
            List<Product> products = new List<Product>();
            //OrderedMultiDictionary<int, Product> products = new OrderedMultiDictionary<int, Product>(true, (x,y)=> y.CompareTo(x));
            maxWeight = 10;
            string []input = new string[]{"beer - weight=3, cost=2",
                    "vodka - weight=8, cost=12",
                    "cheese - weight=4, cost=5",
                    "nuts - weight=1, cost=4",
                    "ham - weight=2, cost=3",
                    "whiskey - weight=8, cost=13"};

            foreach (var line in input)
            {
                string[] elements = line.Split(new char[] { '-', ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                string[] buffer = elements[1].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                int weight = int.Parse(buffer[1]);
                buffer = elements[2].Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                int price = int.Parse(buffer[1]);
                Product product = new Product(elements[0], weight, price);
                products.Add(product);
            }

            // test input Sorting
            /*foreach (var item in products)
            {
                Console.WriteLine("name:{0}, weight:{1}, price{2}", item.Name, item.Weight, item.Price);
            }*/
            products.Sort();
            return products;
        }
    }
}
