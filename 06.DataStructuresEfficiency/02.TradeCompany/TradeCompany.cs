﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wintellect.PowerCollections;

namespace _02.TradeCompany
{
    public class TradeCompany
    {
        static void Main()
        {

            List<string[]> inputs = ReadData();
            OrderedMultiDictionary<double, Article> byPrice = new OrderedMultiDictionary<double, Article>(true);
            foreach (var input in inputs)
            {
                Article article = new Article(input[0], input[1], input[2], Convert.ToDouble(input[3]));
                byPrice.Add(article.Price, article);
            }

            Console.WriteLine("Print all articles");
            foreach (var item in byPrice)
            {
                Console.WriteLine(string.Format("{0}: {1}", item.Key, string.Join(", ", item.Value)));
            }

            OrderedMultiDictionary<double, Article>.View priceInRange = GetPriceRange(48, 55, byPrice);
            Console.WriteLine();
            Console.WriteLine("Print articles in range [48, 55]");
            foreach (var item in priceInRange)
            {
                Console.WriteLine(string.Format("{0}$: {1}", item.Key, string.Join(", ", item.Value)));
            }
        }

        public static OrderedMultiDictionary<double, Article>.View GetPriceRange(double lowPrice, double highPrice, OrderedMultiDictionary<double, Article> byPrice)
        {
            return byPrice.Range(lowPrice, true, highPrice, true);
        }

        private static List<string[]> ReadData()
        {
            List<string[]> inputs = new List<string[]>();
            string sourceFilePath = "../../articles.txt";
            string[] lines = File.ReadAllLines(sourceFilePath);
            foreach (var line in lines)
            {
                var article = line.Split(new char[] { '|', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                inputs.Add(article);
            }

            return inputs;
        }
    }
}
