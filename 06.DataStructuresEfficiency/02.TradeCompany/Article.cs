﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.TradeCompany
{
    public class Article : IComparable
    {
        public string Barcode { get; set; }
        public string Vendor { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }

        public Article(string barcode, string vendor, string title, double price)
        {
            this.Barcode = barcode;
            this.Vendor = vendor;
            this.Title = title;
            this.Price = price;
        }

        public override string ToString()
        {
            string str = string.Format("{0} Vendor:{1}, Barcode:{2} - {3}$", this.Title, this.Barcode, this.Vendor, this.Price);
            return str;
        }

        public int CompareTo(object obj)
        {
            if (obj is Article)
            {
                var article = (Article)obj;

                if (this.Price.CompareTo(article.Price) > 0)
                {
                    return 1;
                }
                else if (this.Price.CompareTo(article.Price) < 0)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
            throw new ArgumentException();
        }
    }
}
