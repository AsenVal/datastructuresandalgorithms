﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.ImplementBiDictionary
{
    public class ImplementBiDictionary
    {
        static void Main()
        {
            var bidictionary = new BiDictionary<string, string, string>(allowDuplicateValues: true);

            bidictionary.Add("Petar", "Ivanov", "JavaScript");
            bidictionary.Add("Georgi", "Stoyanov", "Java");
            bidictionary.Add("Svetlin", "Nakov", "C#");
            bidictionary.Add("Svetlin", "Nakov", "C#");
            bidictionary.Add("Georgi", "Stoyanov", "Coffee");
            bidictionary.Add("Georgi", "Ivanov", "Coffee");
            bidictionary.Add("Presian", "Nakov", "Python");

            Console.WriteLine(string.Join(" ", bidictionary.GetValueByFirstKey("Svetlin")));
            Console.WriteLine(string.Join("; ", bidictionary.GetRecordsByFirstKey("Svetlin")));
            Console.WriteLine(string.Join(" ", bidictionary.GetValueBySecondKey("Ivanov")));
            Console.WriteLine(string.Join("; ", bidictionary.GetRecordsBySecondKey("Ivanov")));
            Console.WriteLine(string.Join(" ", bidictionary.GetValueByFirstAndSecondKey("Georgi", "Stoyanov")));
            Console.WriteLine(string.Join("; ", bidictionary.GetRecordsByFirstAndSecondKey("Georgi", "Stoyanov")));

            Console.WriteLine(bidictionary.Count);

            bidictionary.RemoveByFirstKey("Georgi");
            Console.WriteLine(bidictionary.Count);

            bidictionary.RemoveBySecondKey("Ivanov");
            Console.WriteLine(bidictionary.Count);

            bidictionary.RemoveByFirstAndSecondKey("Svetlin", "Nakov");
            Console.WriteLine(bidictionary.Count);
        }

    }
}
