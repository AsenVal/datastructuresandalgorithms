﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Wintellect.PowerCollections;

namespace _03.ImplementBiDictionary
{
    public class BiDictionary<TKey1, TKey2, TValue>
    {
        public class Entry : IEquatable<Entry>
        {
            public TKey1 Key1 { get; private set; }
            public TKey2 Key2 { get; private set; }
            public TValue Value { get; private set; }

            public Entry(TKey1 key1, TKey2 key2, TValue value)
            {
                this.Key1 = key1;
                this.Key2 = key2;
                this.Value = value;
            }

            public override bool Equals(object obj)
            {
                return Equals(obj as Entry);
            }

            public bool Equals(Entry other)
            {
                return other != null &&
                    this.Key1.Equals(other.Key1) &&
                    this.Key2.Equals(other.Key2) &&
                    this.Value.Equals(other.Value);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int hashCode = 0;

                    hashCode = (hashCode * 397) ^ this.Key1.GetHashCode();
                    hashCode = (hashCode * 397) ^ this.Key2.GetHashCode();
                    hashCode = (hashCode * 397) ^ this.Value.GetHashCode();

                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("{0} {1} {2}", this.Key1, this.Key2, this.Value);
            }
        }

        private readonly MultiDictionary<TKey1, Entry> byKey1 = null;
        private readonly MultiDictionary<TKey2, Entry> byKey2 = null;
        private readonly MultiDictionary<Tuple<TKey1, TKey2>, Entry> byKey1Key2 = null;

        public BiDictionary(bool allowDuplicateValues)
        {
            this.byKey1 = new MultiDictionary<TKey1, Entry>(allowDuplicateValues);
            this.byKey2 = new MultiDictionary<TKey2, Entry>(allowDuplicateValues);
            this.byKey1Key2 = new MultiDictionary<Tuple<TKey1, TKey2>, Entry>(allowDuplicateValues);
        }

        public int Count
        {
            get
            {
                return this.byKey1Key2.KeyValuePairs.Count;
            }
        }

        public void Add(TKey1 key1, TKey2 key2, TValue value)
        {
            var entry = new Entry(key1, key2, value);

            this.byKey1.Add(key1, entry);
            this.byKey2.Add(key2, entry);

            var key1Key2 = new Tuple<TKey1, TKey2>(key1, key2);
            this.byKey1Key2.Add(key1Key2, entry);
        }

        public ICollection<TValue> GetValueByFirstKey(TKey1 key1)
        {
            return this.byKey1[key1].Select(entry => entry.Value).ToArray();
        }

        public ICollection<Entry> GetRecordsByFirstKey(TKey1 key1)
        {
            return this.byKey1[key1].Select(entry => entry).ToArray();
        }

        public void RemoveByFirstKey(TKey1 key1)
        {
            var entries = this.byKey1[key1];

            foreach (var entry in entries)
            {
                this.byKey2.Remove(entry.Key2, entry);

                var key1Key2 = new Tuple<TKey1, TKey2>(entry.Key1, entry.Key2);
                this.byKey1Key2.Remove(key1Key2, entry);
            }

            this.byKey1.Remove(key1);
        }

        public ICollection<TValue> GetValueBySecondKey(TKey2 key2)
        {
            return this.byKey2[key2].Select(entry => entry.Value).ToArray();
        }

        public ICollection<Entry> GetRecordsBySecondKey(TKey2 key2)
        {
            return this.byKey2[key2].Select(entry => entry).ToArray();
        }

        public void RemoveBySecondKey(TKey2 key2)
        {
            var entries = this.byKey2[key2];

            foreach (var entry in entries)
            {
                this.byKey1.Remove(entry.Key1, entry);

                var key1key2 = new Tuple<TKey1, TKey2>(entry.Key1, entry.Key2);
                this.byKey1Key2.Remove(key1key2, entry);
            }

            this.byKey2.Remove(key2);
        }

        public ICollection<TValue> GetValueByFirstAndSecondKey(TKey1 key1, TKey2 key2)
        {
            var key1Key2 = new Tuple<TKey1, TKey2>(key1, key2);

            return this.byKey1Key2[key1Key2].Select(entry => entry.Value).ToArray();
        }

        public ICollection<Entry> GetRecordsByFirstAndSecondKey(TKey1 key1, TKey2 key2)
        {
            var key1Key2 = new Tuple<TKey1, TKey2>(key1, key2);

            return this.byKey1Key2[key1Key2].Select(entry => entry).ToArray();
        }

        public void RemoveByFirstAndSecondKey(TKey1 key1, TKey2 key2)
        {
            var key1Key2 = new Tuple<TKey1, TKey2>(key1, key2);
            var entries = this.byKey1Key2[key1Key2];

            foreach (var entry in entries)
            {
                this.byKey1.Remove(entry.Key1, entry);
                this.byKey2.Remove(entry.Key2, entry);
            }

            this.byKey1Key2.Remove(key1Key2);
        }
    }
}
