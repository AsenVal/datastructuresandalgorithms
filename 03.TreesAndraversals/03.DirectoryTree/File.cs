﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03.DirectoryTree
{
    public class File
    {
        private string name;
        private long sizeInBytes;

        public File(string name, long size)
        {
            this.Name = name;
            this.SizeInBytes = size;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            private set
            {
                if (value == null || value == "")
                {
                    throw new ArgumentException("File name cannot be null or empty");
                }
                this.name = value;
            }
        }
        public long SizeInBytes
        {
            get
            {
                return this.sizeInBytes;
            }
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Size cannot be less than zero");
                }
                this.sizeInBytes = value;
            }
        }
    }
}
