﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02.PrintOddOccurrencesOfString
{
    public class PrintOddOccurrencesOfString
    {
        public static void Main()
        {
            List<string> strings = new List<string>() { "C#", "SQL", "PHP", "PHP", "SQL", "SQL" };
            Dictionary<string, int> occurs = new Dictionary<string, int>();
            foreach (var str in strings)
            {
                if (occurs.ContainsKey(str))
                {
                    occurs[str]++;
                }
                else
                {
                    occurs.Add(str, 1);
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("{");
            foreach (var occur in occurs)
            {
                if (occur.Value % 2 == 1)
                {
                    sb.Append(occur.Key);
                    sb.Append(", ");
                }
            }
            sb.Remove(sb.Length - 2, 2);
            sb.Append("}");
            Console.WriteLine(sb.ToString());
        }
    }
}
