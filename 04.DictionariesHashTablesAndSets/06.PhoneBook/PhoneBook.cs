﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wintellect.PowerCollections;

namespace _06.PhoneBook
{
    public class PhoneBook
    {
        static void Main()
        {
            MultiDictionary<string, string[]> phoneReconds = new MultiDictionary<string, string[]>(true);

            ReadPhonInput(phoneReconds);
            List<string[]> commands = ReadCommandInput();

            foreach (var command in commands)
            {
                if (command[0] == "find")
                {
                    if (command.Length == 2)
                    {
                        Find(command[1], phoneReconds);
                    }
                    else if (command.Length == 3)
                    {
                        Find(command[1], command[2], phoneReconds);
                    }
                }
            }
        }

        private static List<string[]> ReadCommandInput()
        {
            string[] input = System.IO.File.ReadAllLines(@"..\..\comands.txt");
            List<string[]> commands = new List<string[]>();
            foreach (var item in input)
            {
                string[] elements = item.Split(new char[] { '(', ')', ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < elements.Length; i++)
                {
                    elements[i] = elements[i].Trim();
                }
                commands.Add(elements);
            }
            return commands;
        }

        private static void ReadPhonInput(MultiDictionary<string, string[]> phoneReconds)
        {
            
            string[] rows = System.IO.File.ReadAllLines(@"..\..\phones.txt");
            foreach (var row in rows)
            {
                var record = row.Split(new char[] {'|'}, StringSplitOptions.RemoveEmptyEntries);
                string name = record[0].Trim();
                string town = record[1].Trim();
                string phoneNumber = record[2].Trim();
                phoneReconds.Add(name, new string[] { town, phoneNumber });
            }

        }

        public static void Find(string name, MultiDictionary<string, string[]> phoneReconds)
        {
            if (phoneReconds.Keys.Any(key => key.Contains(name)))
            {
                var entry = phoneReconds.Keys.Where(key => key.Contains(name));

                foreach (var item in entry)
                {
                    Console.WriteLine("{0} - {1}", item, phoneReconds[item].ToString());
                }

            }
            else
            {
                Console.WriteLine("No such item in the phone book!");
            }

        }

        public static void Find(string name, string town, MultiDictionary<string, string[]> phoneReconds)
        {
            bool thereIsSuchEntry = false;
            if (phoneReconds.Keys.Any(key => key.Contains(name)))
            {
                var entry = phoneReconds.Keys.Where(key => key.Contains(name));

                foreach (var item in entry)
                {
                    if (phoneReconds[item].ToString().Contains(town))
                    {
                        Console.WriteLine("{0} - {1}", item, phoneReconds[item].ToString());
                        thereIsSuchEntry = true;
                    }
                }
            }
            else
            {
                Console.WriteLine("No such item in the phone book!");
            }

            if (thereIsSuchEntry == false) // because there can be such name but no such town
            {
                Console.WriteLine("No such item in the phone book!");
            }
        } 
    }
}
