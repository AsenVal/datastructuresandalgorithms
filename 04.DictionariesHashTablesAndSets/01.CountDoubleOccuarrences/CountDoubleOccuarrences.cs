﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01.CountDoubleOccuarrences
{
    public class CountDoubleOccuarrences
    {
        public static void Main()
        {
            List<double> numbers = new List<double>() { 3, 4, 4, -2.5, 3, 3, 5, 4, 3, -2.5, 4, -2.5, 3, 3, 5, 6, 6, 3 };
            Dictionary<double, int> occurs = new Dictionary<double, int>();
            foreach (var number in numbers)
            {
                if (occurs.ContainsKey(number))
                {
                    occurs[number]++;
                }
                else
                {
                    occurs.Add(number, 1);
                }
            }

            foreach (var occur in occurs)
            {
                Console.WriteLine("{0} -> {1} times", occur.Key, occur.Value);
            }
        }
    }
}
