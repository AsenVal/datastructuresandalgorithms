﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    namespace _01.FriendsOfPesho
    {
        public class Salaries
        {
            static Graph<int> graph = new Graph<int>();

            static void Main()
            {
                if (ReadInput())
                {
                    long sumDistance;
                    graph.setAllDijkstraDistanceValue(0);

                    sumDistance = (long)graph.getSumOfAllDijkstraDistance();
                    Console.WriteLine(sumDistance);
                }
                else
                {
                    Console.WriteLine(1);
                }
            }


            private static bool ReadInput()
            {
                int C = int.Parse(Console.ReadLine());
                if (C == 1)
                {
                    return false;
                }
                char[] buffer;
                for (int i = 0; i < C; i++)
                {
                    buffer = Console.ReadLine().ToCharArray();
                    for (int j = 0; j < buffer.Length; j++)
                    {
                        if (buffer[j] == 'Y')
                        {
                            graph.AddConnection(i, j, 0, false);
                        }
                    }
                }
                return true;
            }
        }

        public class Node<T> : IComparable
        {
            IList<Edge<T>> _connections;

            public T Name { get; private set; }
            public double DijkstraDistance { get; set; }
            public List<Node<T>> DijkstraPath { get; set; }

            internal IEnumerable<Edge<T>> Connections
            {
                get { return _connections; }
            }

            public Node(T name)
            {
                Name = name;
                _connections = new List<Edge<T>>();
            }

            internal void AddConnection(Node<T> targetNode, double distance, bool twoWay)
            {
                if (targetNode == null)
                {
                    throw new ArgumentNullException("targetNode");
                }

                if (targetNode == this)
                {
                    throw new ArgumentException("Node may not connect to itself.");
                }

                if (distance < 0)
                {
                    throw new ArgumentException("Distance must be positive.");
                }

                _connections.Add(new Edge<T>(this, targetNode, distance));
                if (twoWay)
                {
                    targetNode.AddConnection(this, distance, false);
                }
            }

            public override string ToString()
            {
                return this.Name.ToString();
            }

            public int CompareTo(object obj)
            {
                return this.DijkstraDistance.CompareTo((obj as Node<T>).DijkstraDistance);
            }
        }
        public class Graph<T>
        {
            internal IDictionary<T, Node<T>> Nodes { get; private set; }
            private HashSet<Node<T>> visited;

            public Graph()
            {
                Nodes = new Dictionary<T, Node<T>>();
                visited = new HashSet<Node<T>>();
            }

            public void AddNode(T name)
            {
                var node = new Node<T>(name);
                if (Nodes.ContainsKey(name))
                {
                    throw new ArgumentException(string.Format("Node with name {0} is existing in the graph.", name));
                }
                Nodes.Add(name, node);
            }

            public void AddConnection(T fromNode, T toNode, int distance, bool twoWay)
            {
                if (!Nodes.ContainsKey(fromNode))
                {
                    this.AddNode(fromNode);
                }

                if (!Nodes.ContainsKey(toNode))
                {
                    this.AddNode(toNode);
                }

                Nodes[fromNode].AddConnection(Nodes[toNode], distance, twoWay);
            }

            public void setAllDijkstraDistanceValue(double value)
            {
                foreach (var node in Nodes)
                {
                    node.Value.DijkstraDistance = value;
                }
            }

            public double getSumOfAllDijkstraDistance()
            {
                foreach (var item in Nodes)
                {
                    if (!visited.Contains(item.Value))
                    {
                        employDFS(item.Value);
                    }
                }
                double sum = 0;
                foreach (var node in Nodes)
                {
                    sum += node.Value.DijkstraDistance;
                }

                return sum;
            }
            public void employDFS(Node<T> node)
            {
                visited.Add(node);
                foreach (var item in node.Connections)
                {
                    if (!visited.Contains(item.Target))
                    {
                        employDFS(item.Target);
                    }
                    node.DijkstraDistance += item.Target.DijkstraDistance;
                }

                if (node.DijkstraDistance == 0)
                {
                    node.DijkstraDistance++;
                }

            }
        }

        public class Edge<T> : IComparable
        {
            internal Node<T> Beginning { get; private set; }
            internal Node<T> Target { get; private set; }
            internal double Distance { get; private set; }

            internal Edge(Node<T> begining, Node<T> target, double distance)
            {
                this.Beginning = begining;
                this.Target = target;
                this.Distance = distance;
            }

            public int CompareTo(object obj)
            {
                return this.Distance.CompareTo((obj as Edge<T>).Distance);
            }

            public override string ToString()
            {
                return string.Format("({0} {1}) -> {2}", Beginning, Target, Distance);
            }
        }
    }

}
