﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphLibrary;

namespace _01.FriendsOfPesho
{
    public class FriendsOfPesho
    {
        static HashSet<int> hospitals = new HashSet<int>();
        static Graph<int> graph = new Graph<int>();

        static void Main()
        {
            ReadInput();
            int sumDistance = int.MaxValue;
            foreach (var hospital in hospitals)
            {
                List<Node<int>> nodes = graph.FindShortestDistanceToAllNodes(hospital);
                int currentDistance = 0;
                foreach (var node in nodes)
                {
                    if (!hospitals.Contains(node.Name))
                    {
                        currentDistance += (int)node.DijkstraDistance;
                    }

                }

                if (currentDistance < sumDistance)
                {
                    sumDistance = currentDistance;
                }
            }

            Console.WriteLine(sumDistance);
        }

        private static void ReadInput()
        {
            string[] buffer = Console.ReadLine().Split(' ');
            int N = int.Parse(buffer[0]);
            int M = int.Parse(buffer[1]);
            int H = int.Parse(buffer[2]);            
            buffer = Console.ReadLine().Split(' ');
            int nodeName;
            for (int i = 0; i < H; i++)
            {
                nodeName = int.Parse(buffer[i]);
                hospitals.Add(nodeName);
                graph.AddNode(nodeName);
            }

            for (int i = 0; i < M; i++)
            {
                buffer = Console.ReadLine().Split(' ');
                graph.AddConnection(int.Parse(buffer[0]), int.Parse(buffer[1]), int.Parse(buffer[2]), true);
            }
        }
    }
}
