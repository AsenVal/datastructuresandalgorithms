﻿using GraphLibrary;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _02.Salaries
{
    public class Salaries
    {
        static Graph<int> graph = new Graph<int>();
     
        static void Main()
        {
            if (ReadInput())
            {
                long sumDistance;
                graph.setAllDijkstraDistanceValue(0);
                
                sumDistance = (long)graph.getSumOfAllDijkstraDistance();
                Console.WriteLine(sumDistance);
            }
            else
            {
                Console.WriteLine(1);
            }
        }
               

        private static bool ReadInput()
        {
            int C = int.Parse(Console.ReadLine());
            if (C == 1)
            {
                return false;
            }
            char[] buffer;
            for (int i = 0; i < C; i++)
            {
                buffer = Console.ReadLine().ToCharArray();
                for (int j = 0; j < buffer.Length; j++)
                {
                    if (buffer[j] == 'Y')
                    {
                        graph.AddConnection(i, j, 0, false);
                    }
                }
            }
            return true;
        }
        /*
         static Graph<int> graph = new Graph<int>();
        static List<int> employes = new List<int>();

        static void Main()
        {
            if (ReadInput())
            {
                int sumDistance;
                graph.setAllDijkstraDistanceValue(0);

                foreach (var employ in employes)
                {
                    graph.employBFS(employ);
                }

                sumDistance = (int)graph.getSumOfAllDijkstraDistance();
                Console.WriteLine(sumDistance);
            }
            else
            {
                Console.WriteLine(1);
            }
        }
               

        private static bool ReadInput()
        {
            int C = int.Parse(Console.ReadLine());
            if (C == 1)
            {
                return false;
            }
            char[] buffer;
            for (int i = 0; i < C; i++)
            {
                bool isEmploy = true;
                buffer = Console.ReadLine().ToCharArray();
                for (int j = 0; j < buffer.Length; j++)
                {
                    if (buffer[j] == 'Y')
                    {
                        graph.AddConnection(j, i, 0, false);
                        isEmploy = false;
                    }
                }

                if (isEmploy)
                {
                    employes.Add(i);
                }
            }
            return true;
        }
         */
        /*static int[,] graph;
        static int C;
        static long[] cache;

        static void Main()
        {
            C = int.Parse(Console.ReadLine());
            if (C == 1)
            {
                Console.WriteLine(1);
                return;
            }

            graph = new int[C, C];
            cache = new long[C];
            char[] buffer;
            for (int i = 0; i < C; i++)
            {
                buffer = Console.ReadLine().ToCharArray();
                for (int j = 0; j < buffer.Length; j++)
                {
                    if (buffer[j] == 'Y')
                    {
                        graph[i, j] = 1;
                    }
                }
            }

            long sumOfSalaries = 0;
            for (int i = 0; i < C; i++)
            {
                sumOfSalaries += FindSalary(i);
            }
            Console.WriteLine(sumOfSalaries);
        }

        static long FindSalary(int employee)
        {
            if (cache[employee] > 0)
            {
                return cache[employee];
            }

            long salary = 0;
            for (int i = 0; i < C; i++)
            {
                if (graph[employee, i] == 1)
                {
                    salary += FindSalary(i);
                }
            }
            if (salary == 0)
            {
                salary = 1;
            }
            return salary;
        }*/
    }    
}
