﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiskWinsRiskLoses
{
    class RiskWinsRiskLoses
    {
        static void Main()
        {
            string startNumberString = Console.ReadLine();
            string targetNumberString = Console.ReadLine();
            int n = int.Parse(Console.ReadLine());
            HashSet<string> visited = new HashSet<string>();
            for (int i = 0; i < n; i++)
            {
                visited.Add(Console.ReadLine());
            }

            int numberofCombinations = 0;
            Queue<Tuple<string, int>> queue = new Queue<Tuple<string, int>>();
            queue.Enqueue(new Tuple<string, int>(startNumberString, 0));
            visited.Add(startNumberString);
            while (queue.Count > 0)
            {
                Tuple<string, int> combination = queue.Dequeue();
                numberofCombinations++;
                if (combination.Item1 == targetNumberString)
                {
                    Console.WriteLine(combination.Item2);
                    return;
                }
                
                char[] comb = combination.Item1.ToCharArray();
                for (int i = 0; i < 5; i++)
                {
                    int currentDigit = comb[i] - '0';
                    int digit = currentDigit + 1;
                    if(digit == 10)
                    {
                        digit = 0;
                    }
                    comb[i] = (char)(digit + (int) '0');
                    string currentNode = new string(comb);
                    if (!visited.Contains(currentNode))
                    {
                        queue.Enqueue(new Tuple<string, int>(currentNode, combination.Item2 + 1));
                        visited.Add(currentNode);
                    }
                    digit -= 2;
                    if (digit < 0)
                    {
                        digit = 10 + digit;
                    }

                    comb[i] = (char)(digit + (int)'0');
                    currentNode = new string(comb);
                    if (!visited.Contains(currentNode))
                    {
                        queue.Enqueue(new Tuple<string, int>(currentNode, combination.Item2 + 1));
                        visited.Add(currentNode);
                    }

                    comb[i] = (char)(currentDigit + (int)'0');
                }
            }

            Console.WriteLine(-1);



            /*int[] startNumber = new int[5];
            int[] targetNumber = new int[5];
            int numberOfSteps = 0;
            for (int i = 0; i < 5; i++)
            {
                startNumber[i] = startNumberString[i];
                targetNumber[i] = targetNumberString[i];
            }

            for (int i = 0; i < 5; i++)
            {
                if (Math.Abs(startNumber[i] - targetNumber[i]) <= 5)
                {
                    numberOfSteps += Math.Abs(startNumber[i] - targetNumber[i]);
                }
                else
                {
                    numberOfSteps += 10 - Math.Abs(startNumber[i] - targetNumber[i]);
                }
            }

            Console.WriteLine(numberOfSteps);*/
        }
    }
}
