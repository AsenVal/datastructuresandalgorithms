﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcademyTasks
{
    class AcademyTasks
    {
        static void Main()
        {
            string pleasantness = Console.ReadLine();
            int variety = int.Parse(Console.ReadLine());
            string[] problemsAsString = pleasantness.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int[] problems = new int[problemsAsString.Length];
            for (int i = 0; i < problemsAsString.Length; i++)
            {
                problems[i] = int.Parse(problemsAsString[i]);
            }


            if (problems.Max() - problems.Min() >= variety)
            {
                int min = problems[0];
                int max = problems[0];
                int maxIndex = 0;
                int minIndex = 0;
                for (int i = 0; i < problems.Length; i++)
                {
                    if (problems[i] > max)
                    {
                        max = problems[i];
                        maxIndex = i;
                    }

                    if (problems[i] < min)
                    {
                        min = problems[i];
                        minIndex = i;
                    }

                    if (max - min >= variety)
                    {
                        break;
                    }
                }
                int numberOfProblems = 0;
                if(Math.Min(maxIndex, minIndex) - 1 >=0)
                {
                    if (Math.Abs(maxIndex - minIndex) == 1 && Math.Abs(problems[Math.Max(maxIndex, minIndex)] - problems[Math.Min(maxIndex, minIndex) - 1]) >= variety)
                    {
                        numberOfProblems--;
                    }
                }

                numberOfProblems += 1 + Math.Min(maxIndex, minIndex) / 2 + Math.Min(maxIndex, minIndex) % 2 +
                    Math.Abs(maxIndex - minIndex) / 2 + Math.Abs(maxIndex - minIndex) % 2;

                Console.WriteLine(numberOfProblems);
            }
            else
            {
                Console.WriteLine(problems.Length);
            }
        }

    }
}
