﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageInBottle
{
    class MessageInBottle
    {
        static Dictionary<char, string> codes = new Dictionary<char, string>();
        static List<string> secretMessages = new List<string>();

        static void Main()
        {
            string message = Console.ReadLine();
            string chipher = Console.ReadLine();

            char key = '\0';
            StringBuilder value = new StringBuilder();
            for (int i = 0; i < chipher.Length; i++)
            {
                if (char.IsLetter(chipher[i]))
                {
                    if (key != '\0')
                    {
                        if (!codes.ContainsKey(key))
                        {
                            codes.Add(key, value.ToString());
                        }
                        value.Clear();
                    }
                    key = chipher[i];
                }
                else
                {
                    value.Append(chipher[i]);
                }
            }

            codes.Add(key, value.ToString());
            StringBuilder secretMessage = new StringBuilder();
            FindSecretMessage(message, secretMessage);

            Console.WriteLine(secretMessages.Count);
            secretMessages.Sort();
            for (int i = 0; i < secretMessages.Count; i++)
            {
                Console.WriteLine(secretMessages[i]);
            }
        }

        private static void FindSecretMessage(string message, StringBuilder secretMessage)
        {
            if(message.Length == 0)
            {
                secretMessages.Add(secretMessage.ToString());
                return;
            }
            foreach (var code in codes)
            {
                if (message.StartsWith(code.Value))
                {
                    secretMessage.Append(code.Key);
                    FindSecretMessage(message.Substring(code.Value.Length), secretMessage);
                    secretMessage.Length--;
                }
            }
        }
    }
}
