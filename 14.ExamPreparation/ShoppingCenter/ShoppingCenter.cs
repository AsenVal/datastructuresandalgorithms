﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wintellect.PowerCollections;

namespace ShoppingCenter
{
    class Product : IComparable
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string Producer { get; set; }

        public Product(string name, double price, string producer)
        {
            this.Name = name;
            this.Price = price;
            this.Producer = producer;
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            result.Append("{");
            result.Append(this.Name);
            result.Append(";");
            result.Append(this.Producer);
            result.Append(";");
            result.Append(string.Format("{0:F2}", this.Price));
            result.Append("}");
            return result.ToString();
        }

        public int CompareTo(object obj)
        {
            if (obj is Product)
            {
                Product product = (Product)obj;
                if (this.ToString().CompareTo(product.ToString()) > 1)
                {
                    return 1;
                }
                else if (this.ToString().CompareTo(product.ToString()) < 1)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }

            throw new ArgumentException("This object is not a Product.");
        }
    }

    enum Commands
    {
        AddProduct,
        DeleteProducts,
        FindProductsByName,
        FindProductsByProducer,
        FindProductsByPriceRange
    }

    class ShoppingCenter
    {

        static MultiDictionary<string, Product> byName = new MultiDictionary<string, Product>(true);
        static MultiDictionary<string, Product> byProducer = new MultiDictionary<string, Product>(true);
        static MultiDictionary<Tuple<string, string>, Product> byNameProducer = new MultiDictionary<Tuple<string, string>, Product>(true);
        static OrderedMultiDictionary<double, Product> byPrice = new OrderedMultiDictionary<double, Product>(true,(x,y) => x.CompareTo(y),(x,y)=>0);
        static StringBuilder output = new StringBuilder();

        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                string line = Console.ReadLine();
                int index = line.IndexOf(' ');
                if (index == -1)
                {
                    throw new ArgumentOutOfRangeException("Wrong input - missing command separator - space.");
                }
                string command = line.Substring(0, index);
                line = line.Substring(index + 1);
                string[] parametars = line.Split(';');

                switch (command)
                {
                    case "AddProduct":
                        AddProduct(parametars);
                        break;
                    case "DeleteProducts":
                        DeleteProducts(parametars);
                        break;
                    case "FindProductsByName":
                        FindProductsByName(parametars);
                        break;
                    case "FindProductsByProducer":
                        FindProductsByProducer(parametars);
                        break;
                    case "FindProductsByPriceRange":
                        FindProductsByPriceRange(parametars);
                        break;
                }
            }

            Console.Write(output.ToString());
        }

        private static void FindProductsByProducer(string[] parametars)
        {
            if (byProducer.ContainsKey(parametars[0]))
            {
                OrderedBag<Product> bag = new OrderedBag<Product>();
                var products = byProducer[parametars[0]];
                bag.AddMany(products);

                foreach (var item in bag)
                {
                    output.AppendLine(item.ToString());
                }
            }
            else
            {
                output.AppendLine("No products found");
            }
        }

        private static void FindProductsByPriceRange(string[] parametars)
        {
            var products = byPrice.Range(double.Parse(parametars[0]), true, double.Parse(parametars[1]), true);
            if (products.Count > 0)
            {
                OrderedBag<Product> bag = new OrderedBag<Product>();
                foreach (var product in products)
                {
                    bag.AddMany(product.Value);
                }

                foreach (var item in bag)
                {
                    output.AppendLine(item.ToString());
                }
            }
            else
            {
                output.AppendLine("No products found");
            }
        }

        private static void FindProductsByName(string[] parametars)
        {
            if (byName.ContainsKey(parametars[0]))
            {
                OrderedBag<Product> bag = new OrderedBag<Product>();
                var products = byName[parametars[0]];
                bag.AddMany(products);

                foreach (var item in bag)
                {
                    output.AppendLine(item.ToString());
                }
            }
            else
            {
                output.AppendLine("No products found");
            }

        }

        private static void DeleteProducts(string[] parametars)
        {
            string result = "No products found";
            if (parametars.Length == 1)
            {
                if(byProducer.ContainsKey(parametars[0]))
                {
                    var products = byProducer[parametars[0]];
                    result = products.Count.ToString() + " products deleted";
                    foreach (var product in products)
                    {
                        byName[product.Name].Remove(product);
                        byPrice[product.Price].Remove(product);
                        byNameProducer[new Tuple<string, string>(product.Name, product.Producer)].Remove(product);
                    }
                    byProducer.Remove(parametars[0]);
                }
            }
            else
            {
                var NameProducer = new Tuple<string, string>(parametars[0], parametars[1]);
                if (byNameProducer.ContainsKey(NameProducer))
                {
                    var products = byNameProducer[NameProducer];
                    result = products.Count.ToString() + " products deleted";
                    foreach (var product in products)
                    {
                        if (product.Name == parametars[0])
                        {
                            byName[product.Name].Remove(product);
                            byPrice[product.Price].Remove(product);
                            byProducer[product.Producer].Remove(product);
                        }
                    }
                    
                    byNameProducer.Remove(NameProducer);
                }
            }

            output.AppendLine(result);
        }

        private static void AddProduct(string[] parametars)
        {
            Product product = new Product(parametars[0], double.Parse(parametars[1]), parametars[2]);
            byName.Add(product.Name, product);
            byPrice.Add(product.Price, product);
            byProducer.Add(product.Producer, product);
            byNameProducer.Add(new Tuple<string, string>(product.Name, product.Producer), product);
            output.AppendLine("Product added");
        }
    }
}
