﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriendsInNeed
{
    public class FriendsInNeed
    {
        static HashSet<int> hospitals = new HashSet<int>();
        static Graph<int> graph = new Graph<int>();

        static void Main()
        {
            ReadInput();
            int sumDistance = int.MaxValue;
            foreach (var hospital in hospitals)
            {
                List<Node<int>> nodes = graph.FindShortestDistanceToAllNodes(hospital);
                int currentDistance = 0;
                foreach (var node in nodes)
                {
                    if (!hospitals.Contains(node.Name))
                    {
                        currentDistance += (int)node.DijkstraDistance;
                    }

                }

                if (currentDistance < sumDistance)
                {
                    sumDistance = currentDistance;
                }
            }

            Console.WriteLine(sumDistance);
        }

        private static void ReadInput()
        {
            string[] buffer = Console.ReadLine().Split(' ');
            int N = int.Parse(buffer[0]);
            int M = int.Parse(buffer[1]);
            int H = int.Parse(buffer[2]);            
            buffer = Console.ReadLine().Split(' ');
            int nodeName;
            for (int i = 0; i < H; i++)
            {
                nodeName = int.Parse(buffer[i]);
                hospitals.Add(nodeName);
                graph.AddNode(nodeName);
            }

            for (int i = 0; i < M; i++)
            {
                buffer = Console.ReadLine().Split(' ');
                graph.AddConnection(int.Parse(buffer[0]), int.Parse(buffer[1]), int.Parse(buffer[2]), true);
            }
        }
    }

    public class PriorityQueue<T> : IEnumerable
        where T : IComparable
    {
        private List<T> heap;

        public PriorityQueue()
        {
            heap = new List<T>();
        }

        public PriorityQueue(int size)
        {
            heap = new List<T>(size);
        }

        public int Count
        {
            get
            {
                return this.heap.Count;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return this.heap.Count == 0;
            }
        }

        public void Enqueue(T item)
        {
            this.heap.Add(item);

            int childIndex = this.heap.Count - 1;
            while (childIndex > 0)
            {
                int parentIndex = (childIndex - 1) / 2;
                if (this.heap[parentIndex].CompareTo(this.heap[childIndex]) <= 0)
                {
                    break;
                }

                T swap = this.heap[childIndex];
                this.heap[childIndex] = this.heap[parentIndex];
                this.heap[parentIndex] = swap;

                childIndex = parentIndex;
            }
        }

        public T Dequeue()
        {
            if (this.heap.Count == 0)
            {
                throw new InvalidOperationException("Queue empty.");
            }

            int lastIndex = this.heap.Count - 1;

            T topItem = this.heap[0];
            this.heap[0] = this.heap[lastIndex];
            this.heap.RemoveAt(lastIndex);
            lastIndex--;

            int parentIndex = 0;
            while (true)
            {
                int leftIndex = 2 * parentIndex + 1;
                if (leftIndex > lastIndex)
                {
                    break;
                }

                int swapIndex = leftIndex;
                int rightIndex = leftIndex + 1;
                if (rightIndex <= lastIndex && this.heap[rightIndex].CompareTo(this.heap[leftIndex]) < 0)
                {
                    swapIndex = rightIndex;
                }

                if (this.heap[parentIndex].CompareTo(this.heap[swapIndex]) <= 0)
                {
                    // the parent and the child are in order
                    break;
                }

                T swap = this.heap[swapIndex];
                this.heap[swapIndex] = this.heap[parentIndex];
                this.heap[parentIndex] = swap;

                parentIndex = swapIndex;
            }

            return topItem;
        }

        public T Peek()
        {
            if (this.heap.Count == 0)
            {
                throw new InvalidOperationException("Queue empty.");
            }

            T topItem = this.heap[0];
            return topItem;
        }

        public void Clear()
        {
            this.heap.Clear();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.heap.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public T[] ToArray()
        {
            return this.heap.ToArray();
        }

        public override string ToString()
        {
            return string.Join(", ", this.heap);
        }
    }

    public class Edge<T> : IComparable
    {
        internal Node<T> Beginning { get; private set; }
        internal Node<T> Target { get; private set; }
        internal double Distance { get; private set; }

        internal Edge(Node<T> begining, Node<T> target, double distance)
        {
            this.Beginning = begining;
            this.Target = target;
            this.Distance = distance;
        }

        public int CompareTo(object obj)
        {
            return this.Distance.CompareTo((obj as Edge<T>).Distance);
        }

        public override string ToString()
        {
            return string.Format("({0} {1}) -> {2}", Beginning, Target, Distance);
        }
    }

    public class Node<T> : IComparable
    {
        IList<Edge<T>> _connections;

        public T Name { get; private set; }
        public double DijkstraDistance { get; set; }
        public List<Node<T>> DijkstraPath { get; set; }

        internal IEnumerable<Edge<T>> Connections
        {
            get { return _connections; }
        }

        internal int Count
        {
            get { return _connections.Count; }
        }

        internal Node(T name)
        {
            Name = name;
            _connections = new List<Edge<T>>();
        }

        internal void AddConnection(Node<T> targetNode, double distance, bool twoWay)
        {
            if (targetNode == null)
            {
                throw new ArgumentNullException("targetNode");
            }

            if (targetNode == this)
            {
                throw new ArgumentException("Node may not connect to itself.");
            }

            if (distance < 0)
            {
                throw new ArgumentException("Distance must be positive.");
            }

            _connections.Add(new Edge<T>(this, targetNode, distance));
            if (twoWay)
            {
                targetNode.AddConnection(this, distance, false);
            }
        }

        public override string ToString()
        {
            return this.Name.ToString();
        }

        public int CompareTo(object obj)
        {
            return this.DijkstraDistance.CompareTo((obj as Node<T>).DijkstraDistance);
        }
    }

    public class Graph<T>
    {
        internal IDictionary<T, Node<T>> Nodes { get; private set; }
        private HashSet<Node<T>> visited;

        public Graph()
        {
            Nodes = new Dictionary<T, Node<T>>();
            visited = new HashSet<Node<T>>();
        }

        public void AddNode(T name)
        {
            var node = new Node<T>(name);
            if (Nodes.ContainsKey(name))
            {
                throw new ArgumentException(string.Format("Node with name {0} is existing in the graph.", name));
            }
            Nodes.Add(name, node);
        }

        public void AddConnection(T fromNode, T toNode, int distance, bool twoWay)
        {
            if (!Nodes.ContainsKey(fromNode))
            {
                this.AddNode(fromNode);
            }

            if (!Nodes.ContainsKey(toNode))
            {
                this.AddNode(toNode);
            }

            Nodes[fromNode].AddConnection(Nodes[toNode], distance, twoWay);
        }
        
        public List<Node<T>> FindShortestDistanceToAllNodes(T startNodeName)
        {
            if (!Nodes.ContainsKey(startNodeName))
            {
                throw new ArgumentOutOfRangeException(string.Format("{0} is not containing in the graph.", startNodeName));
            }

            SetShortestDistances(Nodes[startNodeName]);
            List<Node<T>> nodes = new List<Node<T>>();
            foreach (var item in Nodes)
            {
                if (!item.Key.Equals(startNodeName))
                {
                    nodes.Add(item.Value);
                }
            }
            return nodes;
        }

        private void SetShortestDistances(Node<T> startNode)
        {
            PriorityQueue<Node<T>> queue = new PriorityQueue<Node<T>>();

            // set to all nodes DijkstraDistance to PositiveInfinity
            foreach (var node in Nodes)
            {
                if (!startNode.Name.Equals(node.Key))
                {
                    node.Value.DijkstraDistance = double.PositiveInfinity;
                    //queue.Enqueue(node.Value);
                }
            }

            startNode.DijkstraDistance = 0.0d;
            queue.Enqueue(startNode);

            while (queue.Count != 0)
            {
                Node<T> currentNode = queue.Dequeue();

                if (currentNode.DijkstraDistance == double.PositiveInfinity)
                {
                    break;
                }

                foreach (var neighbour in Nodes[currentNode.Name].Connections)
                {
                    double subDistance = currentNode.DijkstraDistance + neighbour.Distance;

                    if (subDistance < neighbour.Target.DijkstraDistance)
                    {
                        neighbour.Target.DijkstraDistance = subDistance;
                        queue.Enqueue(neighbour.Target);
                    }
                }

            }

        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();

            foreach (var node in this.Nodes)
            {
                result.Append(node.Key + " -> ");

                foreach (var conection in node.Value.Connections)
                {
                    result.Append(conection.Target + "-" + conection.Distance + " ");
                }

                result.AppendLine();
            }

            return result.ToString();
        }
    }
}
